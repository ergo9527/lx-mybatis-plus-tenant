package com.lx;

import com.lx.utils.TenantContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 *
 */
@SpringBootApplication
public class TenantApplication {
    public static void main(String[] args) {
        SpringApplication.run(TenantApplication.class, args);
    }

    //将TenantContext交给Spring容器管理
    @Bean
    public TenantContext tenantContext() {
        return new TenantContext();
    }
}
