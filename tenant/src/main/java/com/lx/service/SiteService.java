package com.lx.service;

import com.lx.entity.Site;

import java.util.List;

/**
 * @author Liuxin
 * @date 2021/3/10 13:17
 * @description 类的描述
 */
public interface SiteService {

    List<Site> findAvailableSite(String token);
}
