package com.lx.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lx.entity.Site;
import com.lx.entity.TenantSite;
import com.lx.mapper.SiteMapper;
import com.lx.mapper.TenantSiteMapper;
import com.lx.service.SiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Liuxin
 * @date 2021/3/10 13:17
 * @description 类的描述
 */
@Service
public class SiteServiceImpl implements SiteService {

    @Autowired
    private SiteMapper siteMapper;

    @Autowired
    private TenantSiteMapper tenantSiteMapper;

    @Override
    public List<Site> findAvailableSite(String token) {

        //租户所拥有的站点资源List
        List<Site> sites = new ArrayList<>();

        //根据租户id查询tenant-site List
        List<TenantSite> tsList = tenantSiteMapper.selectList(null);

        //遍历siteIds，根据站点id查找对应站点，并放入sites集合


        for (TenantSite ts : tsList) {

            //准备条件构造器SiteQueryWrapper
            QueryWrapper<Site> siteWrapper = new QueryWrapper<>();
            siteWrapper.eq("site_id", ts.getSiteId());

            sites.add(siteMapper.selectOne(siteWrapper));

        }

        return sites;
    }
}
