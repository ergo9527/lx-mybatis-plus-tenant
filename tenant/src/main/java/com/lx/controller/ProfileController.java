package com.lx.controller;

import com.lx.entity.Profile;
import com.lx.mapper.ProfileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/profile")
public class ProfileController {
 
    @Autowired
    private ProfileMapper profileMapper;
 
    @GetMapping("/findAll/{token}")
    public String findAll(@PathVariable String token) {
 
        //prepareTenantContext(token);
 
        List<Profile> profiles = profileMapper.selectList(null);
        profiles.forEach(System.out::println);
        return "operation complete, the following is the result \n" + profiles.toString();
    }

}