package com.lx.controller;

import com.lx.entity.Site;
import com.lx.service.SiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Liuxin
 * @date 2021/3/10 11:53
 * @description 站点相关接口类1222774306
 */
@RestController
@ResponseBody
@RequestMapping("/site")
public class SiteController {

    @Autowired
    private SiteService siteService;

    /**
     * 查询当前用户(租户)所有可用站点
     *
     * @param token
     * @return
     */
    @GetMapping("/findAll/{token}")
    public String findAvailableSite(@PathVariable String token) {
        List<Site> sites = siteService.findAvailableSite(token);
        sites.forEach(System.out::println);
        return "operation complete, the following is the result \n" + sites.toString();
    }
}
