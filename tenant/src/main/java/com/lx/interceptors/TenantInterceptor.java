package com.lx.interceptors;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.tenant.TenantSqlParser;
import com.lx.handler.MyTenantHandler;
import com.lx.utils.MyTheradLocal;
import com.lx.utils.TenantContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class TenantInterceptor implements HandlerInterceptor {

    @Autowired
    private TenantContext tenantContext;

    @Autowired
    private MyTheradLocal myTheradLocal;

    /**
     * 分页接收器
     */
    @Autowired
    private PaginationInterceptor pi;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {

        //获取token
        String path = httpServletRequest.getRequestURI();
        String token = path.substring(path.lastIndexOf("/") + 1);
        //判断token是否可用
        if (isTokenValid(token)) {
            prepareTenantContext(token);
            return true;
        }

        return false;
    }

    private void prepareTenantContext(String token) {

        TenantSqlParser tenantSqlParser = (TenantSqlParser) pi.getSqlParserList().get(0);
        MyTenantHandler myTenantHandler = (MyTenantHandler) tenantSqlParser.getTenantHandler();
        myTenantHandler.setTenantId(tenantContext.getTenantIdWithToken(token));
    }

    /**
     * 判断token是否有效：是否能根据该token从tenantContext中获得tenantId
     *
     * @param token
     * @return Boolean
     */
    private boolean isTokenValid(String token) {
        return null != tenantContext.getTenantIdWithToken(token);
    }
}