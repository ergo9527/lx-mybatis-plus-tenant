package com.lx.utils;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @author Liuxin
 * @date 2021/3/5 22:04
 * @description 用于绑定用户token和租户id
 */
public class TenantContext {

    private static final Map<String, Object> contextMap = Maps.newConcurrentMap();

    /**
     * 成对存放用户token和租户id
     *
     * @param token
     * @param tenantId
     */
    public void putTokenTenantIdPair(String token, Long tenantId) {
        contextMap.put(token, tenantId);
    }

    /**
     * 根据token获取租户id
     *
     * @param token
     * @return
     */
    public Long getTenantIdWithToken(String token) {
        return (Long) contextMap.get(token);
    }
}
