package com.lx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lx.entity.Profile;
import org.springframework.stereotype.Repository;

/**
 * @author Liuxin
 * @date 2021/3/6 10:27
 * @description 类的描述
 */
@Repository
public interface ProfileMapper extends BaseMapper<Profile> {
}
