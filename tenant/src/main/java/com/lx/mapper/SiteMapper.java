package com.lx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lx.entity.Site;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Liuxin
 * @date 2021/3/10 13:34
 * @description 类的描述
 */
@Mapper
public interface SiteMapper extends BaseMapper<Site> {
}
