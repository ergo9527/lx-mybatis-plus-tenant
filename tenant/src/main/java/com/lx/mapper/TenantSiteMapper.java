package com.lx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lx.entity.TenantSite;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author Liuxin
 * @date 2021/3/10 13:37
 * @description 类的描述
 */
@Mapper
public interface TenantSiteMapper extends BaseMapper<TenantSite> {
}
