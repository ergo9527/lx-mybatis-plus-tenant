package com.lx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lx.entity.User;
import org.springframework.stereotype.Repository;

/**
 * @author Liuxin
 * @date 2021/3/5 22:24
 * @description 类的描述
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}
