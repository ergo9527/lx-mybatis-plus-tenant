package com.lx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.segments.MergeSegments;
import lombok.Data;

import java.io.Serializable;

/**
 * site实体类
 *
 * @author Administrator
 */
@TableName("site")
@Data
public class Site implements Serializable {

    @TableId(value = "site_id", type = IdType.AUTO)
    private Long siteId;//site_id

    @TableField("site_name")
    private String siteName;

    @TableField("site_type")
    private Byte siteType;

}
