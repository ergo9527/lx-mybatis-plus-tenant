package com.lx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * user实体类
 *
 * @author Administrator
 */
@TableName("user")
@Data
public class User implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;//主键

    @TableField("tenant_id")
    private Long tenantId;//租户id:tenant_id

    private String name;//用户姓名


}
