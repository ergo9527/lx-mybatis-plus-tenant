package com.lx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * profile实体类
 *
 * @author Administrator
 */
@TableName("profile")
@Data
public class Profile implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;//id

    @TableField("tenant_id")
    private Long tenantId;//tenant_id

    private String title;//title

    private String content;//content

}
