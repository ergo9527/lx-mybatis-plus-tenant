package com.lx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * tenant实体类
 *
 * @author Administrator
 */
@TableName("tenant")
@Data
public class Tenant implements Serializable {

    @TableId(value = "tenant_id", type = IdType.AUTO)
    private Long tenantId;//tenant_id

    @TableField("tenant_name")
    private String tenantName;//tenant_name


}